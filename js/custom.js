var myOptions = {
    autoplay: true,
    autoplaySpeed: 3000,
    dots: true,
    prevArrow: '<i class="fa fa-chevron-left" class="slick-prev"></i>',
    nextArrow: '<i class="fa fa-chevron-right" class="slick-next"></i>',
    slideToShow: 1,
    
}

$('.mySlider').slick(myOptions)
$('.mySlider-no1').slick(myOptions)


$('.autoplay').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
  });

//   popup 1


$('.open-pop').click(function() {
    $('.popup').css('display', 'flex');
    
    setTimeout( function() {
      $('.popup').addClass('open');
    },0);
    
  });
  
  $('.popup__mask, .popup__close').click(function() {
    
    $('.popup').removeClass('open');
    
    setTimeout( function() {
      $('.popup').css('display', 'none');
    }, 401);
  });

//   popup 2


$('.open-pop2').click(function() {
    $('.popup2').css('display', 'flex');
    
    setTimeout( function() {
      $('.popup2').addClass('open');
    },0);
    
  });
  
  $('.popup__mask2, .popup__close2').click(function() {
    
    $('.popup2').removeClass('open');
    
    setTimeout( function() {
      $('.popup2').css('display', 'none');
    }, 401);
  });

//   contact popup


$('.open-pop3').click(function() {
    $('.popup3').css('display', 'flex');
    
    setTimeout( function() {
      $('.popup3').addClass('open');
    },0);
    
  });
  
  $('.popup__mask3, .popup__close3').click(function() {
    
    $('.popup3').removeClass('open');
    
    setTimeout( function() {
      $('.popup3').css('display', 'none');
    }, 401);
  });

  var timeline = gsap.timeline({delay: 2, repeat: 2, repeatDelay: 1});
timeline.from(".hero-banner h1", {scale: 0.5, y: 50, opacity: 0, duration: 1})
timeline.from(".hero-banner p span", {stagger: 0.2, y: 20, opacity: 0, duration: 1})
timeline.from(".social i", {stagger: 0.4, scale: 0.4, opacity: 0, duration: 1, ease: "elastic.out(1, 0.5)"})